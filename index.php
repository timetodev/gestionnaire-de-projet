<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cahier des charge De projet</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <link href="font-awesome/css/fontawesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    </head>
    
    <body>
        <div class="cdh">
            <p class="p1"><a class="cdh-a" href="#">Créer une tâche <i class="fas fa-plus"></i></a></p>
            <p class="p2"><a class="cdh-b" href="#">Supprimer toutes les tâches <i class="fas fa-ban"></i></a></p>
            <h1>Cahier des Charges</h1>
            <table>
            <tr>
                <th>Tâche(s)</th>
                <th>Crée par</th>
                <th>Date de Création</th>
                <th>Status</th>
            </tr>
            <?php
            require_once('includes/config.php');
            $tache = $bdd->prepare('SELECT * FROM tache');
            $tache->execute();
            /* 1 == Nouvelle tache, 2 == tache en cours, 3 == Tache Annulé, 4 == Tache Terminer,   */  

            while($result = $tache->fetch())
            {
                switch($result['type'])
                {
                    case '1':
                    
                        ?>

                        <tr>
                        <td><?php echo $result['titre'];  ?></td>
                        <td><?php echo $result['user'];  ?></td>
                        <td><?php echo $result['date'];  ?></td>
                        <td class="new"><a href="?page=modal&id=<?php echo $result['id']; ?>" data-toggle="modal" data-target="#myModal"><i class="fas fa-check"></i>Nouvelle Tache</a></td>
                        </tr>
                        <?php
                    break;

                    case '2':
                    
                        ?>

                        <tr>
                        <td></td>
                        <td>Smith</td>
                        <td colspan="2" class="accepted"><i class="fas fa-check"></i> Tache en Cours</td>
                        </tr>""

                        <?php
                    break;

                    case '3':
                    
                        ?>

                        <tr>
                        <td></td>
                        <td>Smith</td>
                        <td class="annuled"><i class="fas fa-check"></i> Tache Annuler</td>
                        </tr>

                        <?php
                    break;

                    case '4':
                    
                        ?>

                        <tr>
                        <td></td>
                        <td>Smith</td>
                        <td class="finish"><i class="fas fa-check"></i> Tache Terminer</td>
                        </tr>

                        <?php
                    break;
                    }
                }
            
                

            ?>
          
            
        </table>
        </div>
        <?php

if(isset($_GET['page']))
{
    if($_GET['page'] == "modal")
    {
        ?>
        
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Modal body..
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

                </div>
            </div>
            </div>

        <?php
    }
}
?>
    </body>

</html>